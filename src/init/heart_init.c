#include <stdio.h>
#include <string.h>

#include "../model/element.h"
#include "../model/parser.h"


void heart_salut(void *elem) {
    printf("\nUm olá do coração ensanguentadooo!!!!");
}


void heart_yell(void *elem) {
    printf("\nSAI DA MINHA VALVA BICÚSPIDE PORR?$#*!!!!");
}


void heart_beat(void *elem) {
    printf("\ntum tum");
}


Element* heart_create() {
    FILE *data_file = open_data_file("data/heart.adv");
    Element *heart = parse_element(data_file, ROOT_LEVEL);

    list_insert(heart->actions, "cumprimentar", heart_salut);
    list_insert(heart->actions, "xingar", heart_yell);
    list_insert(heart->actions, "bater", heart_beat);

    return heart;
}
