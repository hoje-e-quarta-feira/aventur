#include <stdio.h>

#include "../model/element.h"
#include "../model/parser.h"


void left_pulmonary_vein_salut(void *elem) {
    printf("\nYo soy la vena pulmonar izquierda.");
}


void left_pulmonary_vein_carry_oxygen(void *elem) {
    printf("\nÉ pra já! Entregando oxigênio para o coração distribuir.");
}


Element* left_pulmonary_vein_create() {
    FILE *data_file = open_data_file("data/left_pulmonary_vein.adv");
    Element *left_pulmonary_vein = parse_element(data_file, ROOT_LEVEL);

    list_insert(left_pulmonary_vein->actions, "cumprimentar", left_pulmonary_vein_salut);
    list_insert(left_pulmonary_vein->actions, "levar oxigenio", left_pulmonary_vein_carry_oxygen);

    return left_pulmonary_vein;
}
