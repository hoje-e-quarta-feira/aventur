#include <stdio.h>

#include "../model/element.h"
#include "../model/parser.h"


void left_pulmonary_artery_salut(void *elem) {
    printf("\nOi, eu sou uma artéria bem mais legal que aquela outra. Sou a Pulmonar Esquerda.");
}


void left_pulmonary_artery_not_to_clog(void *elem) {
    printf("\nQue missão mais difícil! Acho que não sou capaz...");
}


void left_pulmonary_artery_clog(void *elem) {
    printf("\nDone!");
}


Element* left_pulmonary_artery_create() {
    FILE *data_file = open_data_file("data/left_pulmonary_artery.adv");
    Element *left_pulmonary_artery = parse_element(data_file, ROOT_LEVEL);

    list_insert(left_pulmonary_artery->actions, "cumprimentar", left_pulmonary_artery_salut);
    list_insert(left_pulmonary_artery->actions, "nao entupir", left_pulmonary_artery_not_to_clog);
    list_insert(left_pulmonary_artery->actions, "entupir", left_pulmonary_artery_clog);

    return left_pulmonary_artery;
}
