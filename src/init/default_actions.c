#include <stdio.h>
#include <stdlib.h>

#include "../model/element.h"
#include "../type/hash_table.h"
#include "../type/list.h"


/**
 * Checks whether a given element is able to receive actions
 * 
 * @param elem: the element to check
 */
Bool is_element_in_game(Element *elem) {
    if (elem->is_active) {
        printf("\nparece que %s não estão mais aqui...\n", elem->name);
        return false;
    }
    return true;
}


/**
 * Analyzes an object by outputting its long description, it's
 * name and it's contents list
 * 
 * @param character: a pointer to the game's character
 * @param object: a pointer to an object in game
 */
void analyze(void *character, void *object) {
    Element *obj = (Element*) object;

    if (obj == NULL) {
        printf("\n(ノ°益°)ノ");
        printf("\namigo, me ajuda, pô! se você não mandar nada, eu não vou ter o que analisar\n");
    }
    else {
        printf("\nhmmm... deixa eu ver o que eu sei sobre %s", obj->name);
        printf("\n%s", obj->desc_long);
        printf("\nparece que ele tem: \n");
        element_content_print(obj);
    }
}


/**
 * Tries to kick something. This increases the amount
 * of pressure at the plasmatic membrane
 * 
 * @param character: a pointer to the game's character
 * @param object: a pointer to an object in game
 */
void kick(void *character, void *object) {
    Element *obj = (Element*) object;
    Attribute *attr;

    if (obj->is_place) {
        printf("\nvocê naõ pode chutar lugares, apenas objetos...\n");
        return;
    }

    /**
     * This action is always processed. So, if the element doesn't have
     * this attribute yet, initializes it with a minimum value
     */
    attr = element_get_attribute(obj, "membrane_pressure");
    if (attr == NULL) {
        attr = element_set_attribute(obj, false, "membrane_pressure", "0.1");
    }

    /**
     * Checks whether the membrane pressure of this element has already
     * passed the maximum supportable level. If so, then deactivates the
     * element. It's no more in the game.
     */
    if (attr->value.number >= 1.0) {
        obj->is_active = false;

        printf("\n┐(‘～` )┌");
        printf("\ninfelizmente você arrebentou a membrana plasmática de %s", obj->name);
        printf("\nele está fora do jogo agora\n");
    }
    else {
        attr->value.number = attr->value.number + 0.1;
        printf("\n(×_×)⌒☆");
        printf("\npressão da memebrana em: %0.2f", attr->value.number);
        printf("\nouch! essa doeu até em mim! pobre %s\n", obj->name);
    }
}


/**
 * Eat an object, that is, put it inside the character contents list.
 * This is done only if the object isn't already in the contents list.
 * 
 * @param character: a pointer to the game's character
 * @param object: a pointer to an object in game
 */
void eat(void *character, void *object) {
    Element *self = (Element*) character;
    Element *obj  = (Element*) object;
    Element *elem = list_search(self->contents, obj->name);

    /**
     * We can only eat things we haven't eaten yet
     */
    if (elem == NULL) {
        list_insert(self->contents, obj->name, obj);

        printf("\nyummie, que delícia! só não vai arrebentar a membrana plasmática de tanto comer!");
        printf("\nvocê ainda é um mísero monera, não se esqueça!\n");
    }
    else {
        printf("\n٩(╬ʘ益ʘ╬)۶");
        printf("\noh, comilão! você já comeu isso cara! tá achando o quê? que você é um macrófago?\n");
    }
}


/**
 * Breaks the object by making it invisible
 * 
 * @param character: a pointer to the game's character
 * @param object: a pointer to an object in game
 */
void break_the_fucking(void *character, void *object) {
    Element *obj = (Element*) object;

    /**
     * Makes the object invisible. This means it's still active
     * and ready to execute actions, but you can't no longer
     * see it on the rooms 
     */
    obj->is_visible = false;

    printf("\n(︶︹︺)");
    printf("\nvocê quebrou %s!", obj->name);
    printf("\nacho que você está muito agressivo... ouvi dizer que tem rivotril no estômago, que tal ir lá?");
}
