#include <stdio.h>

#include "../model/element.h"
#include "../model/parser.h"


void right_lung_salut(void *elem) {
    printf("\nHi, there! I'm Mr. Lung. Right Lung!");
}


void right_lung_inspire(void *elem) {
    printf("\nO ar calmamente fluiu para dentro...");
}


void right_lung_expire(void *elem) {
    printf("\nO ar calmamente fluiu para fora...");
}


Element* right_lung_create() {
    FILE *data_file = open_data_file("data/right_lung.adv");
    Element *right_lung = parse_element(data_file, ROOT_LEVEL);

    list_insert(right_lung->actions, "cumprimentar", right_lung_salut);
    list_insert(right_lung->actions, "inspirar", right_lung_inspire);
    list_insert(right_lung->actions, "expirar", right_lung_expire);

    return right_lung;
}
