#include <stdio.h>

#include "../model/element.h"
#include "../model/parser.h"


void left_lung_salut(void *elem) {
    printf("\nÇa va, mon amie? Je suis le poumon gauche!");
}


void left_lung_oxygen(void *elem) {
    printf("\nOh loco, to brisadão no O2 hehe");
}


Element* left_lung_create() {
    FILE *data_file = open_data_file("data/left_lung.adv");
    Element *left_lung = parse_element(data_file, ROOT_LEVEL);

    list_insert(left_lung->actions, "cumprimentar", left_lung_salut);
    list_insert(left_lung->actions, "oxigenar", left_lung_oxygen);

    return left_lung;
}
