#include <stdio.h>

#include "../model/element.h"
#include "../model/parser.h"


void character_salut(void *elem) {
    printf("\nSou a colônia de bactérias mais linda desse jogo!!!");
}


/**
 * Doubles the number of individuals within the main character
 * colony.
 * 
 * @param character: a pointer to the game's character
 */
void character_divide(void *character) {
    Element *self = (Element*) character;
    Attribute *attr;

    attr = element_get_attribute(self, "colony_size");
    if (attr != NULL) {
        printf("\nO tamanho da colônia acaba de dobrar! Confira...");
        attr->value.number = attr->value.number * 2;
    }
}


Element* character_create() {
    FILE *data_file = open_data_file("data/character.adv");
    Element *character = parse_element(data_file, ROOT_LEVEL);

    list_insert(character->actions, "cumprimentar", character_salut);
    list_insert(character->actions, "dividir", character_divide);

    return character;
}
