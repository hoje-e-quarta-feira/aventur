#pragma once

/**
 * Provides useful boolean type for clarity
 * purposes. Chosen over importing stdbool.h
 * to keep compatibility with pre 99 compilers.
 */
typedef enum t_bool {
    false = 0, true = 1
} Bool;
