#pragma once

#include "bool.h"
#include "list.h"

typedef struct list_node {
    char *key;
    void *value;
    struct list_node *next;
} Node;

typedef struct {
    Node *head;
    int length;
} List;

List* list_create();
void list_destroy(List *linked_list);
void list_print(List *linked_list);
void* list_insert(List *linked_list, char *key, void *value);
void* list_search(List *linked_list, char *key);
void* list_remove(List *linked_list, char *key);
void* list_peek(List *linked_list);
