#include <stdio.h>
#include <stdlib.h>

#include "element.h"
#include "../type/bool.h"
#include "../type/hash_table.h"
#include "../type/list.h"


/**
 * Creates an element based on it's unique information
 * 
 * @param name: the name for the element, note it's unique
 * @param desc_short: the short description
 * @param desc_long: the long description
 * @param is_place: a boolean saying whether this is a place 
 * @return a pointer to an allocated element
 */
Element* element_create_from_name(char *name, char *desc_short, char *desc_long, Bool is_place) {
    Element *elem = (Element*) malloc(sizeof(Element));

    elem->name       = name;
    elem->desc_short = desc_short;
    elem->desc_long  = desc_long;
    
    elem->is_active  = false;
    elem->is_visible = false;
    elem->is_known   = false;
    elem->is_place   = is_place;

    /**
     * Everything that essentially is a pointer must start
     * as NULL to prevent shitty values to be yielded
     */
    elem->contents = list_create();
    elem->actions  = list_create();

    /**
     * Instantiate fields according to the element type
     */
    if (elem->is_place) {
        elem->detail.exits = list_create();
    }
    else {
        elem->detail.attributes = hash_table_create(13);
    }

    /**
     * No animation is available for anyone. If this isn't
     * the desired behaviour, set the animation later
     */
    elem->animation = NULL;

    return elem;
}


/**
 * Destroys an element. Note this also destroys the element's
 * associated lists of actions, articles and contents
 * 
 * @param elem: the element to be destroyed
 */
void element_destroy(Element *elem) {

    if (elem != NULL) {
        /**
         * Free complex structures first. This ensures we
         * firstly erases all external data from lists
         */
        list_destroy(elem->contents);
        list_destroy(elem->actions);

        free(elem->name);
        free(elem->desc_short);
        free(elem->desc_long);
        free(elem);  
    }
}


/**
 * Gets an attribute referenced by the param name for a given
 * element. It performs a search in the hash table that contains
 * the element's attributes
 * 
 * @param elem: element to get the attribute from
 * @param name: the key the attribute is referenced with in the table
 * @return a pointer to the attribute found or NULL
 */
Attribute* element_get_attribute(Element *elem, char *name) {
    return hash_table_search(elem->detail.attributes, name);
}


/**
 * Sets an attribute for a given element. This function accepts a param
 * value that is cast to an appropriate type, according to param is_string.
 * Thus, we don't have to worry about type checking when calling this method
 * 
 * @param elem: element in which the attribute will be inserted
 * @param is_string: a bool saying whether the attribute is a string
 * @param key: a unique key to reference this attribute
 * @param value: the attribute value itself, always as a string
 * @return a pointer to the attribute inserted
 */
void* element_set_attribute(Element *elem, Bool is_string, char *key, char *value) {
    Attribute *attr = (Attribute*) malloc(sizeof(Attribute));

    attr->is_string = is_string;
    attr->name = key;

    if (is_string) {
        attr->value.string = value;
    }
    else {
        attr->value.number = atof(value);
    }
    return hash_table_insert(elem->detail.attributes, key, attr);
}


/**
 * A generic function with a common signature, that can be used to print
 * an element-typed object
 * 
 * @param info: the object to cast and print as Element
 */
void element_print(void *info) {
    Element *elem = (Element*) info;

    printf("\n\t[%s name] %s\n", (elem->is_place ? "place" : "object"), elem->name);

    /**
     * Only triggers long description if place isn't known yet
     */
    if (elem->is_place && !elem->is_known){
        printf("\t[long desc] %s\n", elem->desc_long);

        /**
         * When the long description is shown, the element is marked as known
         */ 
        elem->is_known = true;
    }
    else {
        printf("\t[short desc] %s\n", elem->desc_short);
    }
    element_content_print(elem);
    element_actions_print(elem);
    
    if(elem->is_place) {
        element_exits_print(elem);
    }
    else {
        element_attributes_print(elem);
    }
    
}


/**
 * Prints a formatted list of objects that an element contains
 * using articles to reference them correctly
 * 
 * @param elem: the container object
 */
void element_content_print(Element *elem) {
    Element *aux_elem = NULL;
    Node *aux_node = NULL;

    if (elem->contents->head == NULL) {
        printf("\t[content] este %s não contém nada\n", (elem->is_place ? "local" : "objeto"));
    }
    else {
        aux_node = elem->contents->head;
        printf("\t[content] este %s contém ", (elem->is_place ? "local" : "objeto"));
        
        while (aux_node != NULL) {
            aux_elem = (Element*) aux_node->value;
            printf("%s %s, ", aux_elem->articles[0], aux_node->key);
            aux_node = aux_node->next;
        }
        printf("e só.\n");
    }
}


/**
 * Prints a formatted list of actions that an element can take
 * 
 * @param elem: the container object
 */
void element_actions_print(Element *elem) {
    Node *aux_node = NULL;

    if (elem->actions->head == NULL) {
        printf("\t[actions] este %s não pode fazer nada\n", (elem->is_place ? "local" : "objeto"));
    }
    else {
        aux_node = elem->actions->head;
        printf("\t[actions] este %s pode ", (elem->is_place ? "local" : "objeto"));
        
        while (aux_node != NULL) {
            printf("%s, ", aux_node->key);
            aux_node = aux_node->next;
        }
        printf("e só.\n");
    }
}


/**
 * Prints a formatted list of the exits of an element
 * 
 * @param elem: the container object
 */
void element_exits_print(Element *elem) {
    Node *aux_node = NULL;

    if (elem->detail.exits->head == NULL) {
        printf("\t[exits] este local não possui saídas\n");
    }
    else {
        aux_node = elem->detail.exits->head;
        printf("\t[exits] você pode ir para ");
        
        while (aux_node != NULL) {
            printf("%s, ", aux_node->key);
            aux_node = aux_node->next;
        }
        printf("e só.\n");
    }
}


/**
 * Auxiliary function to print each of the lists 
 * in the attributes hash table
 *
 * @param linked_list: one of the lists of the attributes hash table
 */
void element_attributes_list_print(List *linked_list) {
    Node *aux_node = linked_list->head;
    Attribute *aux_attr = NULL;

    while (aux_node != NULL) {
        aux_attr = (Attribute*) aux_node->value;

        if (aux_attr->is_string) {
            printf("\n\t . %s: %s", aux_attr->name, aux_attr->value.string);
        }
        else {
            printf("\n\t . %s: %.2f", aux_attr->name, aux_attr->value.number);
        }
        aux_node = aux_node->next;
    }
}


/**
 * Auxiliary function to print the attributes hash table
 *
 * @param hash_table: the attributes hash_table
 */
void element_attributes_table_print(Hash_Table *hash_table) {
    int i;

    for (i = 0; i < hash_table->capacity; i++) {
        
        if (hash_table->array[i]->head != NULL) {
            element_attributes_list_print(hash_table->array[i]);
        }        
    }
}


/**
 * Prints a formatted list of the attributes of an element
 * 
 * @param elem: the element whose attributes will be printed
 */
void element_attributes_print(Element *elem) {
    printf("\t[attr] este %s tem os seguintes atributos: ", (elem->is_place ? "local" : "objeto"));
    element_attributes_table_print(elem->detail.attributes);
}
