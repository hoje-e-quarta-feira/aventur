#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "../type/bool.h"


/**
 * Opens the data file, in which all the initializing data
 * is contained, so we can parse it and build the correct
 * game objects
 *
 * @param file_name: a string with the file name
 * @return a pointer to the file just opened
 */
FILE* open_data_file(char *file_name) {
    FILE *file = fopen(file_name, "r+");
    
    if (file == NULL) {
        printf("File could not be opened! Are you sure you have the reading rights to it?");
        exit(1);
    }
    return file;
}


/**
 * Parses tabs in files, permitting indents to be used
 * in the adventure description files
 * 
 * @param data_file: file from which to read indents
 * @param level: number of indents we're supposed to read
 */
void parse_indentation(FILE *data_file, int level) {
    long current_position = ftell(data_file);
    fseek(data_file, current_position + level, SEEK_SET);
}


/**
 * Reads an integer from a data stream an interprets it
 * as Bool variable
 * 
 * @param data_file: data stream where the bool wiil be read from
 * @return a Bool value
 */
Bool bool_scan(FILE *data_file, int level) {
    int aux_integer;

    fscanf(data_file, "%d", &aux_integer);
    if (aux_integer == 1) {
        return true;
    }
    else {
        return false;
    }
}


/**
 * Creates a string with the given number of bytes, 
 * not considering additional space to terminating chars
 * 
 * @param  num_bytes: number of bytes to allocate
 * @return a pointer to a string
 */
char* string_create(short num_bytes) {
    char *string = (char*) malloc(num_bytes * sizeof(char));
    
    if (string == NULL) {
        printf("No space left to allocate string!");
        exit(1);
    }
    return string;
}


/**
 * Reads a string from a file stream an removes its trailling
 * new-line character. This function also makes a double check
 * to ensure we actually have new-line character at the end
 * of the string, for safety reasons
 * 
 * @param string: the pointer to where we'll copy the string read
 * @param num_bytes: number of bytes to read from data stream
 * @param data_file: the stream where we'll read the string
 */
void string_scan(char *string, short num_bytes, FILE *data_file) {
    fgets(string, num_bytes, data_file);
    short last_char_index = strlen(string) - 1;

    /**
     * Make sure we have the new-line char at the end of string
     */
    if (string[last_char_index] == '\n') {
        string[last_char_index] = '\0';
    }
}


/**
 * Parses the attributes of an element by reading the data
 * contained in the data_file stream. Then, it makes use
 * of the element_set_attribute function to set them.
 *
 * @param data_file: the stream where we'll read the string
 * @param elem: the element to set the attributes in
 * @param level: current level of indentation in the parsing process
 */
void parse_attributes(FILE *data_file, Element *elem, int level) {
    Bool is_string;

    char *key;
    char *value;
    char *aux_string = string_create(MAX_LEN_SMALL_AUX_STR);

    /**
     * If we have block starting here, this means we have attributes
     * to parse. Thus, we start parsing them until we find a block end
     */
    parse_indentation(data_file, level);
    if (fgetc(data_file) == MARKER_START_BLOCK) {
        /**
         * Trailling new-line char
         */
        fgetc(data_file);
        parse_indentation(data_file, level);

        while (fgetc(data_file) != MARKER_END_BLOCK) {
            /**
             * Reads the marker that indicates whether this is a string
             * or a numeric value
             */
            is_string = bool_scan(data_file, level);
            
            string_scan(aux_string, MAX_LEN_SMALL_AUX_STR, data_file);

            /**
             * Tokenization here helps us separating the attribute key
             * from its associated value
             */
            key = string_create(MAX_LEN_SMALL_AUX_STR);
            strcpy(key, strtok(aux_string, "\t"));
            
            value = string_create(MAX_LEN_SMALL_AUX_STR);
            strcpy(value, strtok(NULL, "\t"));

            /**
             * Sets the attribute read from file. Note this function,
             * internally, casts the string value to the appropriate
             * type, according to param is_string  
             */
            element_set_attribute(elem, is_string, key, value);

            /**
             * Parses the indent of the next line so we're able to safely
             * check if the ending block char is present
             */
            parse_indentation(data_file, level);
        }
    }
    /**
     * Reads additional line-break character after finishing the function
     */
    fgetc(data_file);
}


/**
 * Parses an element recursively. This function is able to
 * create an element and all its contained subelements,
 * in such a way it returns a correct pointer to the topmost
 * element in containing hierarchy
 * 
 * @param data_file: the file stream containing the elements to parse 
 * @return a pointer to a parsed element
 */
Element* parse_element(FILE *data_file, int level) {
    Element *elem = NULL;
    Bool is_place = -1;

    char *name       = string_create(MAX_LEN_NAME);
    char *short_desc = string_create(MAX_LEN_SHORT_DESC);
    char *long_desc  = string_create(MAX_LEN_LONG_DESC);
    char *aux_string = string_create(MAX_LEN_SMALL_AUX_STR);
    char *article_1  = string_create(MAX_LEN_ARTICLE);
    char *article_2  = string_create(MAX_LEN_ARTICLE);

    /**
     * First, reads if this element is place or object using an auxiliary int
     */
    parse_indentation(data_file, level);
    is_place = bool_scan(data_file, level);
    fgetc(data_file);

    /**
     * Next, the basic string components of the elements
     * are read to auxiliary strings
     */
    parse_indentation(data_file, level);
    string_scan(name, MAX_LEN_NAME, data_file);
    
    parse_indentation(data_file, level);
    string_scan(short_desc, MAX_LEN_SHORT_DESC, data_file);
    
    parse_indentation(data_file, level);
    string_scan(long_desc, MAX_LEN_LONG_DESC, data_file);

    /**
     * Create the so-called element, so we can start pushing
     * additional information into it
     */
    elem = element_create_from_name(name, short_desc, long_desc, is_place);

    /**
     * Next we read the articles for this element into an
     * auxiliary string. This is done so we can split it into
     * smaller strings that can be pushed into an array
     */
    parse_indentation(data_file, level);
    string_scan(aux_string, MAX_LEN_SMALL_AUX_STR, data_file);    
    strcpy(article_1, strtok(aux_string, " ,."));    
    strcpy(article_2, strtok(NULL, " ,."));
    elem->articles[0] = article_1;
    elem->articles[1] = article_2;

    /**
     * Next we read the attributes of this particular element, if any available
     */
    parse_attributes(data_file, elem, level);

    /**
     * Finally, we should create all the elements that are contained
     * within this particular element. Only useful for places and
     * elements capable of holding others
     */
    parse_indentation(data_file, level);
    if (fgetc(data_file) == MARKER_START_BLOCK) {
        /*
         * Note that if we entered this condition then we assure at
         * least one contained element is present. Thus, we can start
         * parsing the file until we no longer find a termination
         * character "."
         */
        fgetc(data_file);
        do {
            Element *contained_element = parse_element(data_file, level + 1);
            list_insert(elem->contents, contained_element->name, contained_element);

            /**
             * This auxiliary fgetc is due to an extra trailing new-line char
             */
            fgetc(data_file);
            parse_indentation(data_file, level);
            
        } while (fgetc(data_file) != MARKER_END_BLOCK);
    }
    return elem;
}


/**
 * Reads a file that contains data about the conections between
 * the rooms and parses its information. Each line of that file
 * refer to a place and its exits
 * 
 * @param file_name: a string with the file name
 * @param elements_in_game: the hash table with all elements of the game
 */
void parse_exits(char *file_name, Hash_Table *elements_in_game) {
    FILE *data_file = open_data_file(file_name);
    Element *elem = NULL;
    Element *destination = NULL;

    char *line_read;
    char *origin_name;
    char *destination_name;

    /**
     * This external loop iterates over the lines of the file
     * getting information on the origin
     */
    do {
        line_read = NULL;
        origin_name = NULL;
        destination_name = NULL;
        line_read = string_create(MAX_LEN_BIG_AUX_STR);
        string_scan(line_read, MAX_LEN_BIG_AUX_STR, data_file);

        if (strlen(line_read) > 0) {
            origin_name = string_create(MAX_LEN_NAME);
            origin_name = strtok(line_read, ":");

            elem = (Element*) hash_table_search(elements_in_game, origin_name);

            /**
             * This internal loop iterates in-line over the the words
             * getting information on the destinations
             */
            do {
                destination_name = string_create(MAX_LEN_NAME);
                destination_name = strtok(NULL, ",");

                if (destination_name != NULL) {
                    destination = (Element*) hash_table_search(elements_in_game, destination_name);

                    if (destination != NULL) {
                        list_insert(elem->detail.exits, destination_name, destination);
                    }
                    else {
                        printf("\n\nCouldn't find destination in table\n\n");
                    }
                }

            } while (destination_name != NULL);
        }

    } while (strlen(line_read) > 0);

    fclose(data_file);
}
