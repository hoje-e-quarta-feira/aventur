#include <stdio.h>

#include "../model/element.h"
#include "../type/bool.h"
#include "../type/hash_table.h"

void element_execute_test() {
    Attribute_Value attr_value1;
    attr_value1.string = "doce e levemente amargo";

    Attribute attr1;
    attr1.name = "sabor";
    attr1.is_string = true;
    attr1.value = attr_value1;

    Attribute_Value attr_value2;
    attr_value2.number = 19.9;

    Attribute attr2;
    attr2.name = "preco";
    attr2.is_string = false;
    attr2.value = attr_value2;

    Hash_Table *attributes_table = hash_table_create(5);
    hash_table_insert(attributes_table, attr1.name, &attr1);
    hash_table_insert(attributes_table, attr2.name, &attr2);

    Element *elem = element_create_from_name("tiramisu", "sobremesa", "sobremesa italiana muito gostosa com sabor de cafe", false);
    elem->detail.attributes = attributes_table;

    printf("\n\nelem->name: %s\n", elem->name);
    printf("elem->desc_short: %s\n", elem->desc_short);
    printf("elem->desc_long: %s\n", elem->desc_long);

    Attribute *output_pointer;

    /************** ATRIBUTO 1 **************/

    output_pointer = (Attribute*) hash_table_search(elem->detail.attributes, attr1.name);

    printf("\nATRIBUTO 1:\n");
    printf("attr1->name: %s\n", output_pointer->name);
    printf("attr1->is_string: %s\n", output_pointer->is_string ? "true" : "false");

    if(output_pointer->is_string) {
        printf("attr1->value->string: %s\n", output_pointer->value.string);
    }
    else {
        printf("attr1->value->number: %.2f\n", output_pointer->value.number);
    }

    /************** ATRIBUTO 2 **************/

    output_pointer = (Attribute*) hash_table_search(elem->detail.attributes, attr2.name);

    printf("\nATRIBUTO 2:\n");
    printf("attr2->name: %s\n", output_pointer->name);
    printf("attr2->is_string: %s\n", output_pointer->is_string ? "true" : "false");

    if(output_pointer->is_string) {
        printf("attr2->value->string: %s\n", output_pointer->value.string);
    }
    else {
        printf("attr2->value->number: %.2f\n", output_pointer->value.number);
    }
}